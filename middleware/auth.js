const config = require('config')
const jwt = require("jsonwebtoken")

function auth(req,res,next){
    const token = req.header('x-auth-token');

    //Check for token (on regard le token)
    if(!token)
      return  res.status(401).json({msg: 'No token , authorization danied'});

        try {
            //Verify token ( on verifie le token)
            const decoded = jwt.verify(token,config.get("jwtSecret"))
            
            //Add user from payload (on ajoute le user)
            req.user = decoded;
            next();
        } catch (e) {
            res.status(400).json({msg: 'Token is not valid'})
        }
    
}

module.exports = auth

