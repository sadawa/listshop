const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs")
const config = require("config")
const jwt = require("jsonwebtoken")
//User model( User modele)
const User = require("../../models/Users");

// @route POST api / users
//@desc Register new user (creation d'un nouveau user)
//@acess Public

router.post("/", (req, res) => {
const {name,email,password} = req.body;

// Simple validation (validation simple)
if(!name || !email || !password) {
    return res.status(400).json({msg: 'Please enter all fields'})

}

// Check for existing user (voir si profile existe)
User.findOne({email})
.then(user => {
    if(user) return res.status(400).json({ msg: 'User alreday exists '});

    const newUser = new User({
        name,
        email,
        password
    });

    //create salt & hash (crypte le password)
    bcrypt.genSalt(10, (err,salt) => {
        bcrypt.hash(newUser.password, salt ,(err,hash) => {
            if(err) throw err; 
            newUser.password = hash;
            newUser.save()
                .then( user => {

                    // on utilise les token pour plus de securite et utilise pour les connection (JWT for connecting token)
                    jwt.sign(
                      { id: user.id }, 
                      config.get("jwtSecret"),
                      {expiresIn: 3600},
                      (err,token) => {
                          if(err) throw err;
                          res.json({
                              token, 
                              user:{
                                  id: user.id,
                                  name: user.name,
                                  email: user.email
      
                              }
                          })
                      }
                    )


                })
        })
    })
})

});


module.exports = router;
