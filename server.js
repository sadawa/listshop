const express = require("express");
const mongoose = require("mongoose");
const config = require('config')
const path = require("path")

const app = express();


//bodyparser middleware
app.use(express.json());

// db config
const db = config.get("mongoURI");

// connect mongo 
mongoose
  .connect(db,{
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
  })
  .then(() => console.log("Mongodb atlas is connected.."))
  .catch((err) => console.log(err));

// Route use (utilisation du route)

app.use("/api/items", require("./route/api/items"));
app.use("/api/users", require("./route/api/user"));
app.use("/api/auth", require("./route/api/auth"));

//Server static assets if in prod
if(process.env.NODE_ENV === 'production'){
// Set static folder 
app.use(express.static("client/build"));

app.get("*", (req,res) => {
res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))

})

}

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server started on port ${port}`));
