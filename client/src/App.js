import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import ListShop from "./component/ListShop";
import AppNavbar from "./component/AppNavbar";
import "./App.css";
import ItemModal from "./component/ItemModel";
import { Container } from "reactstrap";

import { Provider } from "react-redux";
import store from "./store";
import {loadUser} from './action/authAction'

class App extends Component {
  componentDidMount(){
    store.dispatch(loadUser())
  }
  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <AppNavbar />
          <Container>
            <ItemModal />
            <ListShop />
          </Container>
        </div>
      </Provider>
    );
  }
}

export default App;
