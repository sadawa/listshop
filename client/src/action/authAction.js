import axios from 'axios'
import {returnErrors} from './errorAction'

import {
    USER_LOADED,
    USER_LOADING,
    AUTH_ERROR,
    LOGIN_FAIL,
    LOGIN_SUCCESS,
    LOGOUT_SUCCESS,
    REGISTER_FAIL,
    REGISTER_SUCCESS
} from './type'

// Check token & load user  (on regard le toke et charge le user)
export const loadUser = () => (dispatch,getState) => {
// User loading (chargement du utlisateur)
dispatch({type: USER_LOADING});

axios
.get('/api/auth/user', tokenConfig(getState))
.then(res =>
  dispatch({
    type: USER_LOADED,
    payload: res.data
  })
)
.catch(err => {
  dispatch(returnErrors(err.response.data, err.response.status));
  dispatch({
    type: AUTH_ERROR
  });
})

}

// Register User ( nouveau ultisateur)
export const register = ({name,email,password}) => dispatch => {
    //Headers
    const config = {
        headers: {
          'Content-type': 'application/json'
        }
      };
    //Request body (requete du bidy)
    const body = JSON.stringify({ name, email, password });

    axios
      .post('/api/users', body, config)
      .then(res =>
        dispatch({
          type: REGISTER_SUCCESS,
          payload: res.data
        })
      )
      .catch(err => {
        dispatch(
          returnErrors(err.response.data, err.response.status, 'REGISTER_FAIL')
        );
        dispatch({
          type: REGISTER_FAIL
        });
    });
};

// Login User
  export const login = ({email,password}) => dispatch => {
    //Headers
    const config = {
        headers: {
          'Content-type': 'application/json'
        }
      };
    //Request body (requete du body)
    const body = JSON.stringify({ email, password });

  axios
    .post('/api/auth', body, config)
    .then(res =>
      dispatch({
        type: LOGIN_SUCCESS,
        payload: res.data
      })
    )
    .catch(err => {
      dispatch(
        returnErrors(err.response.data, err.response.status, 'LOGIN_FAIL')
      );
      dispatch({
        type: LOGIN_FAIL
      });
    });
};

//Logout User
 export const logout = () => {
    return {
        type: LOGOUT_SUCCESS
    }
}

//Setup config /headers and token (on prepare le config du token et du header)
export const tokenConfig = getState => {
// Get token from localstorage (on prend le token qui est dans le localStorage)

const token = getState().auth.token;

//Header (on utilise e header pour sa pour mettre le token )
const config = {
    headers: {
      'Content-type': 'application/json'
    }
  };

//If token , add to header ( si le on trouve le token alors on mette sur le header )
if (token) {
    config.headers['x-auth-token'] = token;
  }

  return config;
};