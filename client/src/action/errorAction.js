
import {
GET_ERRORS,
CLEAR_ERRORS
} from './type'


// Return erros (on retroune erreur)

export const returnErrors = (msg, status, id = null) => {
return{
        type: GET_ERRORS,
        payload: { msg, status ,id }
    }
}
//Clear Errors (on nettoye erreur)
export const clearErrors = () => {

    return {
        type: CLEAR_ERRORS
    }
}