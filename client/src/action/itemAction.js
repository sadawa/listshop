import axios from 'axios';

import { GET_ITEMS, ADD_ITEM, DELETE_ITEM, ITEMS_LOADING } from './type';
import { tokenConfig} from "./authAction"
import {returnErrors} from './errorAction'

export const getItems = () => dispatch => {
  dispatch(setItemsLoading());
  axios.get('/api/items').then(res =>
    dispatch({
      type: GET_ITEMS,
      payload: res.data,
    }))
    .catch(err => dispatch(returnErrors(err.reponse.data, err.reponse.status))
    );
};

export const addItem = item => (dispatch,getState) => {
  axios.post('/api/items', item,tokenConfig(getState)).then(res =>
    dispatch({
      type: ADD_ITEM,
      payload: res.data,
    }),
  ).catch(err => dispatch(returnErrors(err.reponse.data, err.reponse.status))
  );
};

export const setItemsLoading = () => ({
  type: ITEMS_LOADING,
});

export const deleteItem = id => (dispatch,getState) => {
  axios.delete(`/api/items/${id}`, tokenConfig(getState)).then(res =>
    dispatch({
      type: DELETE_ITEM,
      payload: id
    }),
  )
 
};