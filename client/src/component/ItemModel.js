import React, { Component } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import { connect } from "react-redux";
import { addItem } from "../action/itemAction";
import PropTypes from 'prop-types'



export class ItemModel extends Component {
  state = {
    modal: false,
    name: "",
  };

  static propTypes = {
    isAuthenticated: PropTypes.bool,
  }
  toggle = () => {
    this.setState({
      modal: !this.state.modal,
    });
  };
  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  onSubmit = (e) => {
    e.preventDefault();

    const newItem = {
      name: this.state.name,
    };
    this.props.addItem(newItem);

    this.toggle();
  };
  render() {
    return (
      <div>
        {this.props.isAuthenticated ?   <Button
          color="dark"
          style={{ marginBottom: "2rem" }}
          onClick={this.toggle}
        >
          Rajoute
        </Button> : <h4 className="mb-3" ml-4>Please login in to manage items</h4>}
       
        <Modal isOpen={this.state.modal} toglle={this.toggle}>
          <ModalHeader toggle={this.toggle}>Rajoute dans la liste</ModalHeader>
          <ModalBody>
            <Form onSubmit={this.onSubmit}>
              <FormGroup>
                <Label for="item">item</Label>
                <Input
                  type="text"
                  name="name"
                  id="item"
                  placeholder="Rajoute item list"
                  onChange={this.onChange}
                />
                <Button color="dark" style={{ marginTop: "2rem" }} block>
                  Rajoute item
                </Button>
              </FormGroup>
            </Form>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}
const mapStatetoProps = (state) => ({
  item: state.item,
  isAuthenticated: state.auth.isAuthenticated
});

export default connect(mapStatetoProps, { addItem })(ItemModel);
